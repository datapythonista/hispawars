# Welcome to Hispawars!

This project was born with the main objective of generating interest in programming and attracting those who already have a path to continue learning through a competition.

We seek to create a platform to teach programming that can ultimately serve educational organizations (although it is by no means a limit).

The most importantly: **teach and learn** programming **having fun**! :smile:

## Bienvenidos a Hispawars!
Ir a la [versión en español](/es)
